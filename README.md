# Emergence Magazine tropicalizada

Traducciones de ensayos publicados en [Emergence Magazine](https://emergencemagazine.org/).

## Contenidos

- [El maíz sabe mejor en el sistema de honor](el_maiz_sabe_mejor_en_el_sistema_de_honor.md), de [Robin Wall Kimmerer](https://en.wikipedia.org/wiki/Robin_Wall_Kimmerer)
- [Patrones en las olas](patrones_en_las_olas.md), de Aylie Baker.

## Mantenedor

[@elopio](https://gitlab.com/elopio)


## Colaborador+s

- Caro Moya
- JuanJo

---

Traducido con :rainbow: por [JáquerEspeis](https://jaquerespeis.org).

---

